/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
  
    findAll();
    findById(1);
    findAllFilter();
    AddJuego();
    editJuego();
    

});

function findAll()
{
   var token = $("#_token").val();
   $("#findAll").text("Cargando....");
   var url = window.location.protocol + "//" + window.location.host + "/api/juegos";
   $.ajax({
    url: url, // url del recurso
    type: "GET", // podría ser get, post, put o delete.
     headers: {
        'X-Auth-Token':token
       
    },
    data: {}, // datos a pasar al servidor, en caso de necesitarlo
    success: function (r) {
        // aquí trataríamos la respuesta del servidor
     $("#findAll").text(JSON.stringify(r));
    },
    error:function (xhr, ajaxOptions, thrownError){
    $("#findAll").text(JSON.stringify(xhr));
}
}); 
}
function findById(id)
{
     var token = $("#_token").val();
   $("#findById").text("Cargando....");
   var url = window.location.protocol + "//" + window.location.host + "/api/juegos/"+id;
   $.ajax({
    url: url, // url del recurso
    type: "GET", // podría ser get, post, put o delete.
    data: {}, // datos a pasar al servidor, en caso de necesitarlo
    headers: {
        'X-Auth-Token':token
       
    },
    success: function (r) {
        // aquí trataríamos la respuesta del servidor
     $("#findById").text(JSON.stringify(r));
    },
    error:function (xhr, ajaxOptions, thrownError){
         $("#findById").text(JSON.stringify(xhr));
    }
}); 
}

function findAllFilter()
{
    var token = $("#_token").val();
    $("#findAllFilter").text("Cargando....");
   var url = window.location.protocol + "//" + window.location.host + "/api/juegos?limit=3&sort=jue_year&order=asc";
   $.ajax({
    url: url, // url del recurso
    type: "GET", // podría ser get, post, put o delete.
    data: {}, // datos a pasar al servidor, en caso de necesitarlo,
    headers: {
        'X-Auth-Token':token
       
    },
    success: function (r) {
        // aquí trataríamos la respuesta del servidor
     $("#findAllFilter").text(JSON.stringify(r));
    },
    error:function (xhr, ajaxOptions, thrownError){
         $("#findAllFilter").text(JSON.stringify(xhr));
 }
}); 
}

function AddJuego()
{
    var token = $("#_token").val();
    $("#AddJuego").text("Cargando....");
    var url = window.location.protocol + "//" + window.location.host + "/api/juegos";
    
    $.ajax({
    url: url,
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify( { "jue_nombre":"Cuphead", "jue_cantidad_jugadores": 2,"jue_year":2017 } ),
    headers: {
        'X-Auth-Token':token
       
    },
    processData: false,
    success: function( r ){
        $('#AddJuego').html( JSON.stringify( r ) );
    },
    error: function( jqXhr, textStatus, errorThrown ){
         $("#AddJuego").text(errorThrown);
    }
});
}
function editJuego()
{
    var token = $("#_token").val();
    $("#editJuego").text("Cargando....");
    var url = window.location.protocol + "//" + window.location.host + "/api/juegos/2";
    
    $.ajax({
    url: url,
    dataType: 'json',
    type: 'PUT',
    contentType: 'application/json',
//    headers: {"X-HTTP-Method-Override": "PUT"},
    data: JSON.stringify( {"jue_year":2001 } ),
    headers: {
        'X-Auth-Token':token
       
    },
    processData: false,
    success: function( r ){
        $('#editJuego').html( JSON.stringify( r ) );
    },
    error: function( jqXhr, textStatus, errorThrown ){
        $('#editJuego').html( errorThrown );
    }
});
}
