/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
});

function login()
{
   
    
    var nombre = $("#nombre").val();
    var password = $("#password").val();
    
    if(nombre == "" || password =="")
    {
        alert("Por favor ingresar nombre y contraseña ");
        return false;
    }
    
    
    
    var url = window.location.protocol + "//" + window.location.host + "/api/login";
    
    $.ajax({
    url: url,
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify( {"username":nombre,"password":password} ),
   
    processData: false,
    success: function( r ){
       
        var url = window.location.protocol + "//" + window.location.host + "/vistas/log_succ?token="+r.access_token;
        window.location = url;
        
        
        
    },
    error: function( jqXhr, textStatus, errorThrown ){
       
         alert(textStatus+" "+jqXhr.status+": Nombre de usuario o contraseña incorrectas");
    }
});
    
}

function AddJuego()
{
    var token = $("#_token").val();
    $("#AddJuego").text("Cargando....");
    var url = window.location.protocol + "//" + window.location.host + "/api/juegos";
    
    $.ajax({
    url: url,
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify( { "jue_nombre":"Cuphead", "jue_cantidad_jugadores": 2,"jue_year":2017 } ),
    headers: {
        'X-Auth-Token':token
       
    },
    processData: false,
    success: function( r ){
        $('#AddJuego').html( JSON.stringify( r ) );
    },
    error: function( jqXhr, textStatus, errorThrown ){
         $("#AddJuego").text(errorThrown);
    }
});
}