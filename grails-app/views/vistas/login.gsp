<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Bienvenido to Grails</title>
</head>
<body>
   
 <div class="container">
   
      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">Ingresar al sitio</h2>
        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="nombre" required autofocus>
        <input type="password" class="form-control" name="password" id="password" placeholder="password" required>
        

        
          <button class="btn btn-lg btn-primary btn-block" type="button" onclick="login()">Ingresar</button>
          <g:link controller="vistas" action="index">Volver</g:link>
          
      </form>
</div>
</body>
<asset:javascript src="login.js"/>
</html>
