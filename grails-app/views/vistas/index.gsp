<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Bienvenido to Grails</title>
      <asset:stylesheet src="index.css"/>
</head>
<body>
  
   
<div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right">
          <li class="active"><g:link controller="vistas" action="login">Entrar</g:link></li>
          
        </ul>
        <h3 class="text-muted">Octano App</h3>
      </div>

      <div class="jumbotron">
        <h1>Bienvenido a Octano-app ${version}</h1>
        <p class="lead">Para ingresar, favor leer el archivo README.md dentro de la carpeta del sistema.</p>
<!--        <p><a class="btn btn-lg btn-success" href="#" role="button">Sign up today</a></p>-->
      </div>





    </div>
</body>
</html>
