<!doctype html>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>Bienvenido to Grails</title>
    <asset:stylesheet src="index.css"/>
</head>
<body>
    <input type="hidden" name="_token" id="_token" value="${token}">
    <div class="container">
        <div class="header">
            <ul class="nav nav-pills pull-right">
                <li class="active"><g:link controller="vistas" action="logout">Salir</g:link></li>

            </ul>
            <h3 class="text-muted">Octano App</h3>
        </div>
        <p>La API puede tiene los siguientes endpoints</p>
        <div class="alert alert-warning">
            <p>
                Recordar que se tiene que usar el token obtenido cuando entro por el login para usar la API.
                Este token se usa en las cabeceras con el nombre de X-Auth-Token, por lo que cualquier llamada al API deberia poseer esta cabecera o se rechazara.
                <br>
               Su Token es:
            <pre><code>${token}</code> </pre><br>
            
            </p>
        </div>
        <div class="alert alert-warning">
            <p>Todos los output mostrados (a exception del DELETE) son de ejecuciones reales realizadas por JQuery(log_succ.js) al momento de acceder a esta vista , por lo que se ejecutaran nuevamente si se recarga.</p>
        </div>
        <div class="row">
            <h2>GET</h2>  

        </div>


        <div class="row">

            <pre><code>${url}/api/juegos</code> </pre><br>
            <pre><code> curl -i -H "X-Auth-Token:XXXX" localhost:8080/api/juegos</code></pre><br>

                Respuesta:
            <pre>
            <code id="findAll">

            </code>
            </pre>
            <br>
        </div>

        <div class="row">
            <pre><code>${url}/api/juegos/1</code></pre><br>
            <pre><code> curl -i -H "X-Auth-Token:XXXX" localhost:8080/api/juegos/1</code></pre><br>
            Respuesta:
            <pre>
             <code id="findById">

             </code>
            </pre>
            <br>
        </div>

        <div class="row">
            <pre><code>${url}/api/juegos?limit=3&sort=jue_year&order=asc</code></pre><br>
            <pre><code> curl -i -H "X-Auth-Token:XXXX" ${url}/api/juegos?limit=3&sort=jue_year&order=asc</code></pre><br>
            Respuesta:
            <pre>
             <code id="findAllFilter">

             </code>
            </pre>
            <br>
        </div>


        <div class="row">
            <h2>POST</h2>  


        </div>
        <div class="row">
            <pre><code>${url}/api/juegos</code></pre><br>
            <pre><code>curl -H "Content-Type: application/json" -H "X-Auth-Token:XXXX" -X POST ${url}/api/juegos -d "{\"jue_nombre\":\"Cuphead\",\"jue_cantidad_jugadores\":\"2\"}"
</code></pre><br>

                Respuesta:
            <pre>
             <code id="AddJuego">

             </code>
            </pre>
            <br>
        </div>

        <div class="row">
            <h2>PUT</h2>  


        </div>
        <div class="row">
            <pre><code>${url}/api/juegos</code></pre><br>
            <pre><code>curl -H "Content-Type: application/json" -H "X-Auth-Token:XXXX"  -X PUT ${url}/api/juegos/2 -d "{\"jue_nombre\":\"Cambio de Nombre\",\"jue_cantidad_jugadores\":\"2\"}"

</code></pre><br>

                Respuesta:
            <pre>
             <code id="editJuego">

             </code>
            </pre>
            <br>
        </div>

        <div class="row">
            <h2>DELETE</h2>  


        </div>
        <div class="row">
            <pre><code>${url}/api/juegos/2</code></pre><br>
            <pre><code>curl -i -X -H "X-Auth-Token:XXXX" DELETE ${url}/api/juegos/2

</code></pre><br>

                Respuesta:
            <pre>
             <code id="eliminarJuego">
                    HTTP/1.1 204
                    X-Application-Context: application:development
                    Date: Wed, 27 Dec 2017 23:30:42 GMT
             </code>
            </pre>
            <br>
        </div>



    </div>
</body>
<g:javascript src="log_succ.js" />
</html>
