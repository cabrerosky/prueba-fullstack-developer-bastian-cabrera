<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>
   
 <div class="container">

      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">Ingresar al sitio</h2>
        <input type="text" class="form-control" placeholder="Correo" required autofocus>
        <input type="password" class="form-control" placeholder="Password" required>
        
          <input type="checkbox" value="remember-me"> Recordar
        
          <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
      </form>
</div>
</body>
</html>
