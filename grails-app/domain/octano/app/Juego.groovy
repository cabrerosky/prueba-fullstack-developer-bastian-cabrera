package octano.app


class Juego {
    String jue_nombre;
    int jue_year;
    String jue_cantidad_jugadores;
    Long id;
    
    
    static constraints = {
        jue_nombre();
        jue_year();
        jue_cantidad_jugadores();

    }
    //Mapeo los atributos de la clase con sus respectivas columnas de la BD
    static mapping = {
        table 'juego'
        version false
        id column:'id_juego'
        jue_nombre column: 'jue_nombre'
        jue_year column: 'jue_year'
        jue_cantidad_jugadores column: 'jue_cantidad_jugadores'
    }
}
