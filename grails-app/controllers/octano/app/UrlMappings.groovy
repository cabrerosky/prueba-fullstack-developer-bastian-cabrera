package octano.app

// Archivo de Mapeo de la rutas
class UrlMappings {

 
    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/api/juegos"(resources:"juego")

        "/"(controller: "vistas", action: "index")
        "/vistas/log_succ"(controller: "vistas", action: "log_succ")
        "500"(view:'/error')
        "404"(view:'/notFound')
       
        
    }
}
