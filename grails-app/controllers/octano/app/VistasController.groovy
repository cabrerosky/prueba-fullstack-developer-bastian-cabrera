package octano.app

// Controlador de vistas generales
class VistasController {

    def index() { 
    render(view: 'index', model: [version: 0.1])
    }
    def login()
    {   
        
        if(session["token"] != null)
        {
             
             redirect(action: "go_to_login_succ");
        }
        else
        {
            render(view: 'login')
        }
        
//        render(view: 'login')
    }

    def go_to_login_succ()
    {
        if(session["token"] != null)
        {
             render(view: 'log_succ', model:[url:webRequest.baseUrl,token:session["token"]])
         
        }
         else
         {
               redirect(action: "login");
         }
       
    }
    def log_succ()
    {   session["token"] = params.token;
        redirect(action: "go_to_login_succ")
    }
    def logout()
    {
        session["token"] = null;
        redirect(action: "login")
        
    }

 
    
    
}
