package octano.app

import javax.xml.bind.ValidationException
import grails.converters.JSON
import grails.rest.RestfulController

// API RESTful de Juegos, 
class JuegoController extends RestfulController {

    static responseFormats = ['json', 'xml']
    JuegoController() {
        super(Juego)
    }
    // Se le da mas capacidades a la busqueda general, como el limit,sort y order
    def index() {
        
       // Arreglo con respuesta (JSON)
       def response = [];
       // Query a realizar
       String query = " from Juego ";
       // Querys extras que no pueden ir en la query anterior
       def array_query_extra = [:];
       

           if(params.limit != null)
           {   array_query_extra["max"] = params.limit.toInteger();
              
           }
           
           if(params.sort != null)
           {
               query = query+" order by "+params.sort;
           }
            if(params.order != null)
           {
                query = query+" "+params.order;
           }
          
        // Aplico la query
             response = Juego.findAll(query,array_query_extra);
        
        render response as JSON;
    }
    
}
