﻿Octano-App

Realizado por Bastián Andres Cabrera Espinoza

Sistemas utilizados

Grails Version: 3.3.2
Groovy Version: 2.4.13
JVM Version: 1.8.0_151
MySQL


Ejecución

1.- Instalar XAMPP, EasyPHP ó algun servidor que soporte MySQL
2.- Crear base de datos llamada "app" y ejecutar el script /bd/app.sql  para ingresar los datos a la base de datos
3.- El sistema se conectara a la bd mediante el usuario ""@localhost (usuario "") sin password, por ende es necesario asignar los permisos al usuario en cuestion.
4.- Ingresar a la carpeta del programa mediante la Terminal o CMD, luego, ejecutar el comando "grails" (Se instalaran las dependencias del sistema). Luego, ejecutar el comando "run-app"
5.- Para ingresar a la app es necesario abrir su navegador e ingresar la siguiente url: http://localhost:8080
6.- Los usuarios activos para ingresar al sistema son 2:
	- username:'user1',password:'pwd1'
	- username:'user2',password:'pwd2'
7.- El uso de la API se encuentra explicado dentro del sistema, en la vista que se realiza cuando el login es correcto.
8.- La API se puede utilizar mediante cUrl,Postman o cualquier sistema que consuma APIs.
9.- El sistema utiliza Spring Security Rest para la creación de tokens y seguridad de la API.


API REST Juego
- La API del sistema obtiene los datos de la tabla "juego" de la base de datos
- La API solo funciona con un token que es entregado por el sistema.
- El token solo se puede obtener si se realiza un login dentro del sistema con los datos  de usuarios entregados del paso anterior. Para esto, puede utializar cUrl, Postman ó cualquier sistema que realice consumo de apis.


Functiones:

Ejemplos

GET
http://localhost:8080/api/juegos
curl -i -H "X-Auth-Token:XXXX" localhost:8080/api/juegos 

http://localhost:8080/api/juegos/1
curl -i -H "X-Auth-Token:XXXX" localhost:8080/api/juegos/1

http://localhost:8080/api/juegos?limit=3&sort=jue_year&order=asc
curl -i -H "X-Auth-Token:XXXX" http://localhost:8080/api/juegos?limit=3&sort=jue_year&order=asc

POST
http://localhost:8080/api/juegos
curl -H "Content-Type: application/json" -H "X-Auth-Token:XXXX" -X POST http://localhost:8080/api/juegos -d "{\"jue_nombre\":\"Cuphead\",\"jue_cantidad_jugadores\":\"2\"}"

PUT
http://localhost:8080/api/juegos
curl -H "Content-Type: application/json" -H "X-Auth-Token:XXXX"  -X PUT http://localhost:8080/api/juegos/2 -d "{\"jue_nombre\":\"Cambio de Nombre\",\"jue_cantidad_jugadores\":\"2\"}"

DELETE
http://localhost:8080/api/juegos/2
curl -i -X -H "X-Auth-Token:XXXX" DELETE http://localhost:8080/api/juegos/2

 

 Cualquier consulta, favor notificar.
