/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100128
Source Host           : localhost:3306
Source Database       : app

Target Server Type    : MYSQL
Target Server Version : 100128
File Encoding         : 65001

Date: 2017-12-28 02:39:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for authority
-- ----------------------------
DROP TABLE IF EXISTS `authority`;
CREATE TABLE `authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nrgoi6sdvipfsloa7ykxwlslf` (`authority`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of authority
-- ----------------------------
INSERT INTO `authority` VALUES ('1', '0', 'ROLE_USER');

-- ----------------------------
-- Table structure for juego
-- ----------------------------
DROP TABLE IF EXISTS `juego`;
CREATE TABLE `juego` (
  `id_juego` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jue_nombre` varchar(255) NOT NULL,
  `jue_year` int(4) NOT NULL,
  `jue_cantidad_jugadores` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_juego`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of juego
-- ----------------------------
INSERT INTO `juego` VALUES ('1', 'Resident Evil', '1996', '1');
INSERT INTO `juego` VALUES ('2', 'Silent Hill', '2001', '1');
INSERT INTO `juego` VALUES ('3', 'Final Fantasy', '1987', '1');
INSERT INTO `juego` VALUES ('4', 'Counter-Strike', '1999', '16');
INSERT INTO `juego` VALUES ('5', 'Quake 2', '1997', '16');
INSERT INTO `juego` VALUES ('6', 'Megaman', '1987', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `password_expired` bit(1) NOT NULL,
  `username` varchar(255) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `account_expired` bit(1) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '0', '\0', 'user1', '\0', '$2a$10$x9XoU0kHNIwvrCmnOGKHE.jXMaNqL84YzV.LZ.B4E0/L9qbzDJpNi', '\0', '');
INSERT INTO `user` VALUES ('2', '0', '\0', 'user2', '\0', '$2a$10$BCU8Tu6c0D.yd/M0NGJCa.jV/Xqx92yjKk3Vxp5/TyC3fQsX6luwO', '\0', '');

-- ----------------------------
-- Table structure for user_authority
-- ----------------------------
DROP TABLE IF EXISTS `user_authority`;
CREATE TABLE `user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_id`),
  KEY `FKgvxjs381k6f48d5d2yi11uh89` (`authority_id`),
  CONSTRAINT `FKgvxjs381k6f48d5d2yi11uh89` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`),
  CONSTRAINT `FKpqlsjpkybgos9w2svcri7j8xy` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_authority
-- ----------------------------
INSERT INTO `user_authority` VALUES ('1', '1');
